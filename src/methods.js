const Discord = require('./methods/discord')
const Facebook = require('./methods/facebook')
const Linkedin = require('./methods/linkedin')
const Locales = require('./methods/locales')
const Msg = require('./methods/msg')
const Post = require('./methods/post')
const Skype = require('./methods/skype')
const Thread = require('./methods/thread')
const Twitter = require('./methods/twitter')
const User = require('./methods/user')

module.exports = {
  Discord,
  Facebook,
  Linkedin,
  Locales,
  Msg,
  Post,
  Skype,
  Thread,
  Twitter,
  User
}
